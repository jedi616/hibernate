package keith.hibernate;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.junit.Test;

import keith.domain.jpa.entity.StudentM;

/**
 * 
 * @author Keith F. Jayme
 *
 */
public class UsingAnnotationTest
{
    @Test
    public void test()
    {
        Configuration configuration = new Configuration().configure();
        SessionFactory sessionFactory = configuration.buildSessionFactory();
        Session session = null;
        Transaction transaction = null;
        try
        {
            // Using openSession()
            session = sessionFactory.openSession();

            // Truncate Table student_m
            Query query = session.createSQLQuery("SET FOREIGN_KEY_CHECKS = 0");
            query.executeUpdate();
            query = session.createSQLQuery("truncate table student_m");
            query.executeUpdate();
            System.out.println("student_m table truncated...");
            System.out.println();
            query = session.createSQLQuery("SET FOREIGN_KEY_CHECKS = 1");
            query.executeUpdate();

            // Create
            transaction = session.beginTransaction();
            StudentM newStudent = new StudentM();
            newStudent.setName("Jimmy");
            session.save(newStudent);
            newStudent = new StudentM();
            newStudent.setName("Ana");
            session.save(newStudent);
            newStudent = new StudentM();
            newStudent.setName("Tom");
            session.save(newStudent);
            transaction.commit();

            // Retrieve or List
            Criteria criteria = session.createCriteria(StudentM.class);
            List<StudentM> students = getSuppressedList(criteria);
            displayStudents(students);

            // Update
            transaction = session.beginTransaction();
            StudentM studentToUpdate = session.get(StudentM.class, 3L);
            studentToUpdate.setName("John");
            session.save(studentToUpdate);
            transaction.commit();

            // Delete
            transaction = session.beginTransaction();
            StudentM studentToDelete = session.get(StudentM.class, 2L);
            session.delete(studentToDelete);
            transaction.commit();

            // Retrieve or List
            criteria = session.createCriteria(StudentM.class);
            students = getSuppressedList(criteria);
            displayStudents(students);

            // Search for Student's name
            query = session.createQuery("Select s From StudentM s Where s.name = :name1");
            query.setParameter("name1", "Jimmy");
            students = getSuppressedList(query);
            displayStudents(students);
        }
        catch (Exception e)
        {
            System.out.println("Error: " + e.getMessage());
        }

        finally
        {
            if (session != null)
            {
                session.flush();
                session.close();
            }
        }
    }

    private void displayStudents(List<StudentM> students)
    {
        for (StudentM student : students)
        {
            System.out.print("id=" + student.getStudentId() + "; ");
            System.out.println("name=" + student.getName() + "; ");
        }
        System.out.println();
    }

    @SuppressWarnings("unchecked")
    private List<StudentM> getSuppressedList(Criteria criteria)
    {
        return criteria.list();
    }

    @SuppressWarnings("unchecked")
    private List<StudentM> getSuppressedList(Query query)
    {
        return query.list();
    }
}
