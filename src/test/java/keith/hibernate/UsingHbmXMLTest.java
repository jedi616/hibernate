package keith.hibernate;

import java.util.List;

import keith.hibernate.entity.Student;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.junit.Test;

/**
 * 
 * @author Keith F. Jayme
 *
 */
public class UsingHbmXMLTest
{
    @Test
    public void test()
    {
        Configuration configuration = new Configuration().configure();
        SessionFactory sessionFactory = configuration.buildSessionFactory();
        Session session = null;
        Transaction transaction = null;
        try
        {
            // Using openSession()
            session = sessionFactory.openSession();

            // Truncate Table student_m
            Query query = session.createSQLQuery("SET FOREIGN_KEY_CHECKS = 0");
            query.executeUpdate();
            query = session.createSQLQuery("truncate table student_m");
            query.executeUpdate();
            System.out.println("student_m table truncated...");
            System.out.println();
            query = session.createSQLQuery("SET FOREIGN_KEY_CHECKS = 1");
            query.executeUpdate();

            // Create
            transaction = session.beginTransaction();
            Student newStudent = new Student();
            newStudent.setName("Jimmy");
            session.save(newStudent);
            newStudent = new Student();
            newStudent.setName("Ana");
            session.save(newStudent);
            newStudent = new Student();
            newStudent.setName("Tom");
            session.save(newStudent);
            transaction.commit();

            // Retrieve or List
            Criteria criteria = session.createCriteria(Student.class);
            List<Student> students = getSuppressedList(criteria);
            displayStudents(students);

            // Update
            transaction = session.beginTransaction();
            Student studentToUpdate = session.get(Student.class, 3L);
            studentToUpdate.setName("John");
            session.save(studentToUpdate);
            transaction.commit();

            // Delete
            transaction = session.beginTransaction();
            Student studentToDelete = session.get(Student.class, 2L);
            session.delete(studentToDelete);
            transaction.commit();

            // Retrieve or List
            criteria = session.createCriteria(Student.class);
            students = getSuppressedList(criteria);
            displayStudents(students);

            // Search for Student's name
            query = session.createQuery("Select s From Student s Where s.name = :name1");
            query.setParameter("name1", "Jimmy");
            students = getSuppressedList(query);
            displayStudents(students);
        }
        catch (Exception e)
        {
            System.out.println("Error: " + e.getMessage());
        }

        finally
        {
            if (session != null)
            {
                session.flush();
                session.close();
            }
        }
    }

    private void displayStudents(List<Student> students)
    {
        for (Student student : students)
        {
            System.out.print("id=" + student.getStudentId() + "; ");
            System.out.println("name=" + student.getName() + "; ");
        }
        System.out.println();
    }

    @SuppressWarnings("unchecked")
    private List<Student> getSuppressedList(Criteria criteria)
    {
        return criteria.list();
    }

    @SuppressWarnings("unchecked")
    private List<Student> getSuppressedList(Query query)
    {
        return query.list();
    }
}
