package keith.hibernate.dao;

import keith.domain.jpa.entity.StudentM;
import keith.hibernate.dao.generic.GenericDaoImpl;

/**
 * 
 * @author Keith F. Jayme
 *
 */
public class StudentDaoImpl extends GenericDaoImpl<StudentM, Long> implements StudentDao
{
    private static StudentDao dao;

    static
    {
        dao = new StudentDaoImpl();
    }

    public static StudentDao getInstance()
    {
        return dao;
    }
}
