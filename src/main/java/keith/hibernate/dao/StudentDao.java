package keith.hibernate.dao;

import keith.domain.jpa.entity.StudentM;
import keith.hibernate.dao.generic.GenericDao;

/**
 * 
 * @author Keith F. Jayme
 *
 */
public interface StudentDao extends GenericDao<StudentM, Long>
{
}
