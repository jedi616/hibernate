package keith.hibernate.rawdao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * 
 * @author Keith F. Jayme
 *
 */
public class ConnectionUtil
{
    protected static SessionFactory sessionFactory;

    static
    {
        Configuration configuration = new Configuration().configure();
        sessionFactory = configuration.buildSessionFactory();
    }

    public static Session getCurrentSession()
    {
        return sessionFactory.getCurrentSession();
    }
}
