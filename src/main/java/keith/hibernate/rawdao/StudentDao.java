package keith.hibernate.rawdao;

import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import keith.domain.jpa.entity.StudentM;

/**
 * 
 * @author Keith F. Jayme
 *
 */
public class StudentDao
{
    private static StudentDao studentDao;

    static
    {
        studentDao = new StudentDao();
    }

    public static StudentDao getInstance()
    {
        return studentDao;
    }

    public List<StudentM> findAll() throws Exception
    {
        Session session = ConnectionUtil.getCurrentSession();
        Transaction transaction = session.beginTransaction();
        try
        {
            Criteria criteria = session.createCriteria(StudentM.class);
            @SuppressWarnings("unchecked")
            List<StudentM> students = criteria.list();
            transaction.commit();
            return students;
        }
        catch (Exception e)
        {
            transaction.rollback();
            throw e;
        }
    }

    public List<StudentM> findAll(String hqlQuery, Map<String, Object> params) throws Exception
    {
        Session session = ConnectionUtil.getCurrentSession();
        Transaction transaction = session.beginTransaction();
        try
        {
            Query query = session.createQuery(hqlQuery);
            for (Map.Entry<String, Object> entry : params.entrySet())
            {
                String key = entry.getKey();
                Object value = entry.getValue();
                query.setParameter(key, value);
            }
            @SuppressWarnings("unchecked")
            List<StudentM> students = query.list();
            transaction.commit();
            return students;
        }
        catch (Exception e)
        {
            transaction.rollback();
            throw e;
        }
    }

    public StudentM findOne(Long studentId) throws Exception
    {
        Session session = ConnectionUtil.getCurrentSession();
        Transaction transaction = session.beginTransaction();
        try
        {
            StudentM student = session.get(StudentM.class, studentId);
            transaction.commit();
            return student;
        }
        catch (Exception e)
        {
            transaction.rollback();
            throw e;
        }
    }

    public StudentM save(StudentM student) throws Exception
    {
        Session session = ConnectionUtil.getCurrentSession();
        Transaction transaction = session.beginTransaction();
        try
        {
            StudentM savedStudent = (StudentM) session.merge(student);
            transaction.commit();
            return savedStudent;
        }
        catch (Exception e)
        {
            transaction.rollback();
            throw e;
        }
    }

    public void delete(Long studentId) throws Exception
    {
        Session session = ConnectionUtil.getCurrentSession();
        Transaction transaction = session.beginTransaction();
        try
        {
            StudentM studentToDelete = session.get(StudentM.class, studentId);
            session.delete(studentToDelete);
            transaction.commit();
        }
        catch (Exception e)
        {
            transaction.rollback();
            throw e;
        }
    }

    public void truncateTable() throws Exception
    {
        Session session = ConnectionUtil.getCurrentSession();
        Transaction transaction = session.beginTransaction();
        try
        {
            Query query = session.createSQLQuery("SET FOREIGN_KEY_CHECKS = 0");
            query.executeUpdate();
            query = session.createSQLQuery("truncate table student_m");
            query.executeUpdate();
            query = session.createSQLQuery("SET FOREIGN_KEY_CHECKS = 1");
            query.executeUpdate();
            transaction.commit();
        }
        catch (Exception e)
        {
            transaction.rollback();
            throw e;
        }
    }
}
